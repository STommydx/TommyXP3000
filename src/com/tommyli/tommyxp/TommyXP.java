package com.tommyli.tommyxp;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class TommyXP extends JavaPlugin{
	
	File databasefile;
	FileConfiguration databaseconfig;
	
	@Override
	public void onDisable(){
		PluginDescriptionFile pdf = this.getDescription();
		getLogger().info(pdf.getName() + " has been disabled.");
	}
	
	@Override
	public void onEnable(){
		PluginDescriptionFile pdf = this.getDescription();
		getLogger().info(pdf.getName() + " has been enabled.");
		if (!setupBase()) getServer().getPluginManager().disablePlugin(this);
		getServer().getPluginManager().registerEvents(new XPBottleListener(this), this);
	}
		
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (commandLabel.equalsIgnoreCase("txp")){
			if (sender instanceof Player){
				Player s = (Player) sender;
				if (args.length == 0){
					s.sendMessage(ChatColor.GREEN + "You have " + ChatColor.GOLD + getXP(s) + ChatColor.GREEN + " XP.");
					return true;
				}else if (args[0].equalsIgnoreCase("set")){
					Player tar;
					int xp;
					try{
						if (args.length == 2){
							tar = s;
							xp = Integer.parseInt(args[1]);
						}else{
							tar = s.getServer().getPlayer(args[1]);
							xp = Integer.parseInt(args[2]);
						}
						if (tar != null){
							setXP(tar, xp);
							s.sendMessage(ChatColor.GREEN + "Set " + ChatColor.GOLD + tar.getDisplayName() + ChatColor.GREEN + " XP to " + ChatColor.GOLD + xp + ChatColor.GREEN + ".");
							return true;
						}else s.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
					}catch(NumberFormatException e){
						s.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
					}
				}else if (args[0].equalsIgnoreCase("add")){
					Player tar;
					int xp;
					try{
						if (args.length == 2){
							tar = s;
							xp = Integer.parseInt(args[1]);
						}else{
							tar = s.getServer().getPlayer(args[1]);
							xp = Integer.parseInt(args[2]);
						}
						if (tar != null){
							setXP(tar, getXP(tar) + xp);
							s.sendMessage(ChatColor.GREEN + "Added " + ChatColor.GOLD + xp + ChatColor.GREEN + " XP to " + ChatColor.GOLD + tar.getDisplayName() + ChatColor.GREEN + ".");
							return true;
						}else s.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
					}catch(NumberFormatException e){
						s.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
					}
				}else if (args[0].equalsIgnoreCase("remove")){
					Player tar;
					int xp;
					try{
						if (args.length == 2){
							tar = s;
							xp = Integer.parseInt(args[1]);
						}else{
							tar = s.getServer().getPlayer(args[1]);
							xp = Integer.parseInt(args[2]);
						}
						if (tar != null){
							setXP(tar, getXP(tar) - xp);
							s.sendMessage(ChatColor.GREEN + "Removed " + ChatColor.GOLD + xp + ChatColor.GREEN + " XP from " + ChatColor.GOLD + tar.getDisplayName() + ChatColor.GREEN + ".");
							return true;
						}else s.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
					}catch(NumberFormatException e){
						s.sendMessage(ChatColor.YELLOW + "[Warning] Syntax error.");
					}
				}else if (args[0].equalsIgnoreCase("reload")) {
					loadCon();
					s.sendMessage(ChatColor.GREEN + "Reload complete.");
				}else if (args.length == 1){
					Player tar = s.getServer().getPlayer(args[0]);
					if (tar != null) {
						s.sendMessage(ChatColor.GOLD + tar.getDisplayName() + ChatColor.GREEN + " have " + ChatColor.GOLD + getXP(tar) + ChatColor.GREEN + " XP.");
						return true;
					}else s.sendMessage(ChatColor.YELLOW + "[Warning] Player not online/exist.");
				}
			}
		}
		return false;
	}
	
	private boolean setupBase(){
		databasefile = new File(getDataFolder(), "database.yml");
		databaseconfig = new YamlConfiguration();
		if (!databasefile.exists()){
			databasefile.getParentFile().mkdirs();
			try {
				databasefile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				getLogger().info("[Warning] Database file creation failed, plugin disabling...");
				return false;
			}
		}
		if (!loadCon()) return false;
		return true;
	}
	
	public int getXP(Player p){
		loadCon();
		return databaseconfig.getInt(p.getName());
	}
	
	public void setXP(Player p, int xp){
		loadCon();
		databaseconfig.set(p.getName(), xp);
		saveCon();
		f5Bottle(p);
	}
	
	private boolean loadCon(){
			try {
				databaseconfig.load(databasefile);
			} catch (IOException
					| InvalidConfigurationException e) {
				e.printStackTrace();
				getLogger().info("[Warning] Can't load database file!!!!!");
				return false;
			}
			return true;
	}
	
	private boolean saveCon(){
		try {
			databaseconfig.save(databasefile);
		} catch (IOException e) {
			e.printStackTrace();
			getLogger().info("[Warning] Can't save database file!!!!!");
			return false;
		}
		return true;
	}
	
	public void f5Bottle(Player p){
		ItemStack bottle = new ItemStack(384, -2);
		ItemMeta m = bottle.getItemMeta();
		m.setDisplayName(ChatColor.GREEN + "XP: " + ChatColor.GOLD + getXP(p));
		List<String> lo = Arrays.asList(ChatColor.YELLOW + "Click to open XP shop.");
		m.setLore(lo);
		bottle.setItemMeta(m);
		p.getInventory().setItem(8, bottle);
	}

}
