package com.tommyli.tommyxp;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class XPBottleListener implements Listener{
	
	private TommyXP plugin;

	public XPBottleListener(TommyXP p) {
		plugin = p;
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		plugin.setXP(p, plugin.getXP(p) - 10);
		p.sendMessage(ChatColor.RED + "-10XP: Death");
	}
	
	@EventHandler
	public void onBottleDrop(PlayerDropItemEvent e){
		if (e.isCancelled()) return;
		ItemStack is = e.getItemDrop().getItemStack();
		if (is == null) return;
		if (is.getItemMeta().getDisplayName().startsWith(ChatColor.GREEN + "XP: "));{
			e.setCancelled(true);
			plugin.f5Bottle(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onBottleMove(InventoryClickEvent e){
		if (e.isCancelled()) return;
		if ((e.getSlot() == 8 || e.getHotbarButton() == 8) && e.getWhoClicked().getGameMode() != GameMode.CREATIVE) e.setCancelled(true);
	}
	
	@EventHandler
	public void onBottleThrow(PlayerInteractEvent e){
		if (e.getItem() == null) return;
		if ((e.getItem().getItemMeta().getDisplayName().startsWith(ChatColor.GREEN + "XP: ")) && e.getPlayer().getGameMode() != GameMode.CREATIVE){
			e.setCancelled(true);
			plugin.f5Bottle(e.getPlayer());
			if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				//for future shop inv use
			}
		}
	}
	
	

}
